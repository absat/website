import { User } from "~/types/Auth";

export default  defineNuxtRouteMiddleware(()=>{
    
    const userData = useCookie<User>('user');
    const auth = useAuth();
    const accessToken = useToken();

    if(userData.value){
        auth.value.isAuthenticated = true;
        auth.value.userData = userData.value
        console.log(`Welcome from Initial middleware `,auth.value,accessToken.value);
    }else{
        console.log(`Welcome from visitor`);
    }

    

});