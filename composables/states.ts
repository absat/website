import { Auth } from "~/types/Auth";

export const useCurrentLang = () => useState<string>('lang',() =>{
    const currentLang = useCookie('lang');
    if( currentLang.value ){
        return currentLang.value;
    }else{
        return 'ar';
    } 
})
export const useAuth = ()=>useState<Auth>('auth', () => ({
    isAuthenticated: false,
    userData: null
}));

export const useToken = ()=>useState<string | null>('token', () =>{
    const t = useCookie('jwt_token');
    if( t.value ){
        return t.value;
    }else{
        return null;
    }
});