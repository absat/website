// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    'nuxt-icon',
    ['@nuxtjs/google-fonts',{
    families: {
      Tajawal: true,
      // Inter: [400, 700],
      // 'Josefin+Sans': true,
      // Lato: [100, 300],
      // Raleway: {
      //   wght: [100, 400],
      //   ital: [100]
      // },
    }
    }],
  ],
  css: [
    '~/assets/css/root.css',
    '~/assets/css/buttons.css',
    '~/assets/css/main.css',
    '~/assets/css/style_ar.css'
  ],
  runtimeConfig: {
    public: {
        APP_SALES_ID:process.env.APP_SALES_ID,
        APP_SALES_URL:process.env.APP_SALES_URL,
        API_AUTH_URL:process.env.API_AUTH_URL,
        baseURL:
            process.env.NODE_ENV !== "production"
              ? process.env.DEV_BASE_URL
              : process.env.PRO_BASE_URL,
    },
  }, 
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  googleFonts: {
    download: false
    // Options
  }
})
