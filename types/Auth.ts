export type User = {
        _id: string;
        email?: string;
        username: string;
        mobile:string;
        name: string;
        jobTitle?: string;
        user_class_id?: string;
        default_app_id?: string;
        my_apps?:string[];
        userSetting?:string[];
        tenant_id?: string;
        company_id?: string;
        branch_id?: string;
}
export type Auth = {
    isAuthenticated: boolean;
    userData? : User | null;
}