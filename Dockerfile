# Use the official Node.js 14 image as the base image
FROM node:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the working directory
COPY package*.json ./

# Install project dependencies
#RUN npm ci --quiet
RUN npm install --quiet


# Copy the entire project directory to the working directory
COPY . .

# Build the Nuxt.js application
RUN npm run build

# Generate the static files
#RUN npm run generate

# expose the host and port 3000 to the server
ENV HOST 0.0.0.0
ENV PORT 3020

EXPOSE 3020

# run the build project with node
ENTRYPOINT ["node", ".output/server/index.mjs"]

# ENTRYPOINT ["node", ".output/public"]

# ENTRYPOINT [ "npm", "start" ]


