export type  Tenant = {
    userId: string;
    tenantName : string;
    tenantPhone? : string | null;
    tenantFax? : string | null;
    tenantCountry? : string | null;
    tenantCity? : string | null;
    contactPersonName? : string | null;
    contactPersonMobile? : string | null;
    username : string ;
    domainName?: string | null;
    databaseName?: string | null;
    companies :{
                name:{
                    text: string;
                    lang: string;
                }[];
                desc?:{
                    text?: string | null;
                    lang?: string | null;
                }[];
                commercialRegisteration?: string | null ;
                vatNumber?: string | null ;
                logo?:string | null;
                mainAddress?: {
                    text: string | null;
                    lang: string | null;
                }[],
                mainPhone? : string | null;
                mainEmail? : string | null;
                websiteUrl?:  string | null;
                isActive: Boolean;
                branchCount: Number;
                empCount: Number;
                mainCurrency: String;
                hasVat: Boolean;
                country: String;
                branches:{
                    branchCode: string | null;
                    name: {
                        text: string | null;
                        lang: string | null;
                    }[];
                    desc: {
                        text: string | null;
                        lang: string | null;
                    }[];

                }[],
                settings: {
                    generalSettings: {
                        name: string;
                        value?: string;
                        _id:string;
                    }[];
                    invoice: {
                        name: string;
                        value?: string;
                        _id:string; 
                    }[]
                }
            }[];
    languages :{
        langName : string
        langCode : string 
    }[];
    setting:{
            generalSettings: {
                name : string ;
                value : string;
            }[],
            appSetting :{
                app: string;
                setting:[] 
            } [];
    }
    
}  

export type TenantForm = {
        name?:string | null;
        branchCount?: number | null;
        branchName?: string | null;
        empCount?: number ;
        currency?: string | null;
        country?: string | null;
        hasVat?: boolean ;
}