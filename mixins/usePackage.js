import {ref,watch,reactive} from 'vue';
const usePackage = () => {
    const period = ref('Monthly');
    const plansAndItems = () => (
    {
        plans:{
            basic:{
                title:'BASIC',
                desc: 'فئة الاعمال الصغيرة',
                price: period.value === 'Monthly' ? "49" :"490",
                period: period.value
            },
            plus:{
                title:'PLUS',
                desc: 'فئة الاعمال التي ننصح بها',
                price: period.value === 'Monthly' ? "99" :"990",
                period: period.value
            },
            permium:{
                title:'PERMIUM',
                desc: 'فئة الاعمال والمنشات الكبيرة',
                price: period.value === 'Monthly' ? "199" :"1990",
                period: period.value
            },
        },
        spec:{
            branchCount:{
                title:  "عدد الفروع",
                basicPlan: '1',
                plusPlan:'1',
                premiumPlan: '3',
            },
            companyCount:{
                title: "عدد الشركات",
                basicPlan: '1',
                plusPlan:'1',
                premiumPlan: '3',
            },
            userCount:{
                title: "عدد المستخدمين",
                basicPlan: '1',
                plusPlan:'3',
                premiumPlan: '5',
            },
            invoiceAndQutation:{
                title: "الفواتير وعروض الأسعار",
                basicPlan: '100',
                plusPlan:'500',
                premiumPlan: 'غير محدود',
            },
            pointOfSale:{
                title: "نقاط بيع سحابية",
                basicPlan: false,
                plusPlan: false,
                premiumPlan: true,
            },
            customersCount:{
                title: "عدد العملاء غير محدود",
                basicPlan: '100',
                plusPlan: '500',
                premiumPlan: 'غير محدود',
            },
            multiCurrency:{
                title: "متعدد العملات",
                basicPlan: 'عملة واحدة',
                plusPlan: 'غير محدود',
                premiumPlan: 'غير محدود',
            },
            supportPdfXml:{
                title: "دعم الفواتير بصيغة pdf,xml",
                basicPlan: true,
                plusPlan: true,
                premiumPlan: true,
            },
            vatReports:{
                title: "إنشاء تلقائي لإقرارات الضريبية",
                basicPlan: true,
                plusPlan: true,
                premiumPlan: true,
            },
            supportHyaa:{
                title: "متوافق مع  هيئة الزكاة والضريبة والجمارك",
                basicPlan: true,
                plusPlan: true,
                premiumPlan: true,
            },
            supportAPI:{
                title: "API للفوترة الإلكترونية",
                basicPlan: true,
                plusPlan: true,
                premiumPlan: true,
            },
            supportSendSmsEmail:{
                title: "ارسال الفاتورة وعروض الاسعار للعميل عبر البريد والرسائل النصية",
                basicPlan: false,
                plusPlan: true,
                premiumPlan: true,
            },
            salesManCount:{
                title: "مناديب المبيعات",
                basicPlan: '3',
                plusPlan: '5',
                premiumPlan: 'غير محدود',
            },
            productCount:{
                title: "المنتجات والخدمات",
                basicPlan: '100',
                plusPlan: '500',
                premiumPlan: 'غير محدود',
            },
            
        }
    });
    const basicFeatured = [
        "عدد الفروع واحد",
        "عدد 1 مستخدم",
        "آنشئ وأسل الفواتير",
        "عدد العملاء غير محدود",
        "متعدد العملات",
        "دعم الفواتير بصيغة pdf,xml",
        "إنشاء تلقائي لإقرارات الضريبية",
        "متوافق مع  هيئة الزكاة والضريبة والجمارك",
    ];
    
    const plusFeatured = [
    "عدد الفروع واحد",
    "عدد لانهائي من المستخدمين",
    "آنشئ وأسل الفواتير",
    "عدد العملاء غير محدود",
    "متعدد العملات",
    "دعم الفواتير بصيغة pdf,xml",
    "إنشاء تلقائي لإقرارات الضريبية",
    "متوافق مع  هيئة الزكاة والضريبة والجمارك",
    "API للفوترة الإلكترونية",
    "ارسال الفاتورة وعروض الاسعار للعميل عبر البريد الالكتروني والوتس اب والرسائل النصية",
    "عدد المناديب غير محدود",
    "آنشئ وأسل عروض الاسعار",
    ];
    
    const premiumFeatured = [
    "عدد 3 فروع",
    "عدد 3 نشاط تجاري",
    "عدد لانهائي من المستخدمين",
    "آنشئ وأسل الفواتير",
    "عدد العملاء غير محدود",
    "متعدد العملات",
    "دعم الفواتير بصيغة pdf,xml",
    "إنشاء تلقائي لإقرارات الضريبية",
    "متوافق مع  هيئة الزكاة والضريبة والجمارك",
    "API للفوترة الإلكترونية",
    "ارسال الفاتورة وعروض الاسعار للعميل عبر البريد الالكتروني والوتس اب والرسائل النصية",
    "عدد المناديب غير محدود",
    "آنشئ وأسل عروض الاسعار",
    ];
    
    const packagesListHandler = () => {
        return [
            {
                packageName: "BASIC",
                packageDesc: " للأعمال والشركات الصغيرة التي لديها فرع واحد",
                packagePrice: period.value === 'Monthly' ? "49" :"490",
                packageFeatured: basicFeatured
            },
            {
                packageName: "PLUS" ,
                packageDesc: "للشركات  المتوسطة التي لديها فرع او اكثر",
                packagePrice: period.value === 'Monthly' ? "99" : "990" ,
                packageFeatured: plusFeatured
            },
            {
                packageName: "PREMIUM",
                packageDesc: "للمنشآت الكبيرة التي لديها اكثر من نشاط تجاري واكثر من فرع",
                packagePrice: period.value === 'Monthly' ? "199" : "1990",
                packageFeatured: premiumFeatured
            }
        ];
    }
    const packagesList = ref(packagesListHandler());
    
    const changePeriod = ()=>{
        if(period.value === 'Yearly'){
            period.value = 'Monthly' 
        }else{   
            period.value = 'Yearly' 
        }
        
        const newList = packagesListHandler();
        packagesList.value.length = 0 ;
        packagesList.value.push(...newList);
        console.log(newList);
    }

    watch(()=>packagesList.value,()=>{
        console.log('packagesList Changed');
    })
    watch(()=>period.value,()=>{
        console.log(' Period Changed ');
      //  plansAndItems.value = {...plansAndItems}
    })
    watch(()=>plansAndItems.value,()=>{
        console.log('plansAndItems Changed',plansAndItems.value)
    })
    return {plansAndItems,packagesList,period,changePeriod}
}

export default usePackage;