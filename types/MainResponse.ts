export type MainResponse = {
    status: number,
    message: string ,
    data: Object | null,
}