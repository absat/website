import axios from "axios";

const tryLogin = async(userName:string,password:string) =>{
    const runtimeConfig = useRuntimeConfig();
    const _data = {
        email: userName,
        password: password
    }
    try{
        console.log(_data);
        const result = await axios.post(`${runtimeConfig.public.API_AUTH_URL}/users/login`,_data);
        console.log(result);
        if(result){
            return result.data;
        }
    }catch(err){
        console.log(err);
    }

    return false;
}

export {tryLogin};